package com.sda.hibernate.runner;

import com.sda.hibernate.employee.Employee;
import com.sda.hibernate.project.Project;
import com.sda.hibernate.util.HibernateUtil;
import org.hibernate.Session;

public class ApplicationRunner {

    public static void main(String[] args) {
        System.out.println("Hibernate many to many (Annotation)");
        Session session = HibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();

        Project firstProject = new Project("Virtual Reality");
        Project secondProject = new Project("Banking");
        Project thirdProject = new Project("Retail");
        Project lastProject  = new Project("Factory");

        Employee firstEmployee = new Employee("Cristi");
        firstEmployee.getProjects().add(firstProject);
        firstEmployee.getProjects().add(secondProject);
        session.save(firstEmployee);

        Employee secondEmployee = new Employee("Marcus");
        secondEmployee.getProjects().add(thirdProject);
        secondEmployee.getProjects().add(lastProject);
        session.save(secondEmployee);

        session.getTransaction().commit();
        System.out.println("Done");
    }

}
